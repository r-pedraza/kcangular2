import {Injectable} from '@angular/core'
 import {Http, Headers, RequestOptionsArgs} from '@angular/http'

 @Injectable()
 export class LoginService {
   private baseURL = 'http://localhost:3000/apiv1/';
   private accessToken : string = null;

   constructor(private http: Http) {
     console.log("Me inyectaaaaaan!" + http);
   };

   login(userName: string, password: string) {
    return this.http.post(this.baseURL + 'login', {userName: userName, password: password}).do((response) => {
      this.accessToken = response.json().data;
    });
   }

   getProductos() {
     let options : RequestOptionsArgs = {};
     let headers : Headers = new Headers();
     headers.set('Authorization', 'Bearer ' + this.accessToken);
     options.headers = headers;
     return this.http.get(this.baseURL + 'products', options);
   }
 }
