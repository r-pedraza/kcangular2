import {Component} from '@angular/core'
import {LoginService} from './login.service';


@Component({
  selector:'login',
  template:`
    <label>User Name</label>
    <input name='email' #email/>
    <label>password</label>
    <input name='password' #password/>
    <button (click)='doLogin(email.value, password.value)'>Login</button>
    <button (click)='doGetInfo()'>Haz algo</button>
  `,
  providers: [LoginService],
  styleUrls:['./resources/bootstrap-3.3.6-dist/css'],
})

export class Login{
  constructor(private loginService : LoginService) {}

  doLogin(userName: string, password: string) {
    this.loginService.login(userName,password).subscribe((response) => {
      //console.log(response.json().data);
      alert("LoginService ya tiene el token");
    });
  }

  doGetInfo(){
    this.loginService.getProductos().subscribe((response) => {
      console.log(response.json())
    });
  }
}
