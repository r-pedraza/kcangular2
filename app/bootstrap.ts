///<reference path="./../typings/main.d.ts"/>
import 'reflect-metadata'
import 'es6-shim'
import 'rxjs'
import 'zone.js/dist/zone'
import 'zone.js/dist/long-stack-trace-zone'
import { bootstrap } from '@angular/platform-browser-dynamic'
import {AppComponent} from './main'
import {Login} from './login/login.component';
import {HTTP_PROVIDERS} from '@angular/http'

bootstrap(Login, [HTTP_PROVIDERS])
